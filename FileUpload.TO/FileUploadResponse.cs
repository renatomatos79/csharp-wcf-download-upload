﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FileUpload.TO
{
    [DataContract]
    public class FileUploadResponse
    {
        [DataMember]
        public string FileName { get; set; }
        [DataMember]
        public long FileSize { get; set; }
        [DataMember]
        public string URL { get; set; }
    }

    [DataContract]
    public class FileUploadContentResponse
    {
        [DataMember]
        public string Content { get; set; }
        [DataMember]
        public bool Exists { get; set; }
    }
}