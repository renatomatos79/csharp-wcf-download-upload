﻿using FileUpload.Framework;
using FileUpload.TO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileUpload
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtFileName.Text = openFileDialog1.FileName;
            }
        }

        private void GetAllFiles()
        {
            using (var svc = new FileUploadServiceReference.FileUploadClient())
            {
                var files = svc.GetFiles();
                dataGridView1.AutoGenerateColumns = false;
                dataGridView1.DataSource = files.ToList();
                dataGridView1.Refresh();
            }
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFileName.Text))
            {
                MessageBox.Show("Arquivo não informado!");
                return;
            }
            if (System.IO.File.Exists(txtFileName.Text) == false)
            {
                MessageBox.Show("Arquivo não encontrado!");
                return;
            }
            var fileInfo = new FileInfo(txtFileName.Text);
            var request = new FileUpload.TO.FileUploadRequest
            {
                Content = HelperFile.ToBase64(txtFileName.Text),
                FileName = fileInfo.Name
            };
            using (var svc = new FileUploadServiceReference.FileUploadClient())
            {
                svc.Upload(request);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            GetAllFiles();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            GetAllFiles();
        }

        private void btnDonwload_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Selecione uma linha");
                return;
            }
            var row = dataGridView1.SelectedRows[0].DataBoundItem as FileUpload.TO.FileUploadResponse;
            using (var svc = new FileUploadServiceReference.FileUploadClient())
            {
                var content = svc.Download(row.FileName).Content;
                saveFileDialog1.FileName = row.FileName;
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    HelperFile.SaveToDisk(saveFileDialog1.FileName, content);
                    MessageBox.Show("Arquivo salvo com sucesso!");
                }
            }
        }
    }
}
