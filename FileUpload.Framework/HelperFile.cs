﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileUpload.Framework
{
    public static class HelperFile
    {
        public static string ToBase64(string imageFileName)
        {
            if (!System.IO.File.Exists(imageFileName))
            {
                return null;
            }
            byte[] imageArray = System.IO.File.ReadAllBytes(imageFileName);
            return Convert.ToBase64String(imageArray, Base64FormattingOptions.None);
        }

        public static void SaveToDisk(string fileName, string base64)
        {
            var bytes = Convert.FromBase64String(base64);
            System.IO.File.WriteAllBytes(fileName, bytes);
        }
    }
}
