﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using FileUpload.TO;
using System.Web;
using System.IO;
using FileUpload.Framework;

namespace FileUpload.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IFileUpload
    {
        public FileUploadContentResponse Download(string fileName)
        {
            var result = new FileUploadContentResponse();
            var folder = HttpContext.Current.Server.MapPath("~/Upload");
            var file = System.IO.Path.Combine(folder, fileName);
            result.Exists = System.IO.File.Exists(file);
            if (result.Exists)
            {
                result.Content = HelperFile.ToBase64(file);
            }           
            return result;
        }

        public List<FileUploadResponse> GetFiles()
        {
            var result = new List<FileUploadResponse>();
            var folder = HttpContext.Current.Server.MapPath("~/Upload");
            foreach (string file in Directory.EnumerateFiles(folder, "*.pdf"))
            {
                var fileName = System.IO.Path.Combine(folder, file);
                var info = new FileInfo(fileName);
                result.Add(new FileUploadResponse
                {
                    FileName = info.Name,
                    FileSize = info.Length,
                    URL = "http://localhost:62838/Upload/" + info.Name
                });
            }
            return result;
        }

        public void Upload(FileUploadRequest request)
        {
            var folder = HttpContext.Current.Server.MapPath("~/Upload");
            var file = System.IO.Path.Combine(folder, request.FileName);
            HelperFile.SaveToDisk(file, request.Content);
        }
    }
}
