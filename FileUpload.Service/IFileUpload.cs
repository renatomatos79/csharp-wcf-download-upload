﻿using FileUpload.TO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace FileUpload.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IFileUpload
    {
        [OperationContract]
        void Upload(FileUploadRequest request);
        [OperationContract]
        List<FileUploadResponse> GetFiles();
        [OperationContract]
        FileUploadContentResponse Download(string fileName);
    }    
}
